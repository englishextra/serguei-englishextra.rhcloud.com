/*jslint node: true */
/*jslint esversion: 6 */
module.exports = {
	entry : "./diy/libs/serguei-eaststreet/js/bundle.js",
	output : {
		path : "./diy/libs/serguei-eaststreet/js",
		filename : "bundle.min.js"
	}
};
