# serguei-englishextra.surge.sh

<https://serguei-englishextra.surge.sh>

## About Surge

<https://github.com/sintaxi/surge>

Static web publishing for Front-End Developers.

This is the CLI client for the surge.sh hosted service. It’s what gets installed when you run `npm install -g surge`.

This CLI library manages access tokens locally and handles the upload and subsequent reporting when you publish a project using surge.

## Publish project

```
npm install -g surge
```

You will have to login / register (admin.txt).

Do `surge` in root folder where `index.html` resides:

```
D:
cd server\serguei-englishextra.rhcloud.com\diy
surge
```

Output:
```
surge
D:\server\serguei-englishextra.rhcloud.com\diy
serguei-englishextra.surge.sh
============
Success! Published and running at my-project.surge.sh
```

You will have to confirm project path with `Enter` and manually change randomly generated subdomain name!

## Remove domain

Best thing to do would be to run surge list, pick the ones you no longer need and then "teardown" that project:

`surge teardown serguei-englishextra.surge.sh`

Tearing down a project is the equivalent of deleting the project.

<https://github.com/sintaxi/surge/issues/167>

## Usage

Run `surge --help` to see the following overview of the `surge` command...

```
  Surge – Single-command web publishing. (v0.17.6)

  Usage:
    surge [options]

  Options:
    -p, --project       path to projects asset directory (./)
    -d, --domain        domain of your project (<random>.surge.sh)
    -a, --add           adds user to list of collaborators (email address)
    -r, --remove        removes user from list of collaborators (email address)
    -V, --version       show the version number
    -h, --help          show this help message

  Shorthand usage:
    surge [project path] [domain]

  Additional commands:
    surge whoami        show who you are logged in as
    surge logout        expire local token
    surge login         only performs authentication step
    surge list          list all domains you have access to
    surge teardown      tear down a published project

  Guides:
    Getting started     surge.sh/help/getting-started-with-surge
    Custom domains      surge.sh/help/adding-a-custom-domain
    Additional help     surge.sh/help

  When in doubt, run surge from within your project directory.
```