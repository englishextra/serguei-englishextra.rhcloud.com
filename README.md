# serguei-englishextra.rhcloud.com

*A Private Teacher of English in Tushino (the homepage)*

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0197588863ee49cca554133d693aaee2)](https://www.codacy.com/app/englishextra/serguei-englishextra-rhcloud-com?utm_source=github.com&utm_medium=referral&utm_content=englishextra/serguei-englishextra.rhcloud.com&utm_campaign=badger)
[![Travis](https://img.shields.io/travis/englishextra/serguei-englishextra.rhcloud.com.svg)](https://github.com/englishextra/serguei-englishextra.rhcloud.com)

## On-line

 - [the website](https://serguei-englishextra.rhcloud.com/)

## Dashboard

<https://openshift.redhat.com/app/console/application/57c07b5889f5cfaf8d000184-app>

## Production Push URL

```
ssh://57c07b5889f5cfaf8d000184@serguei-englishextra.rhcloud.com/~/git/serguei.git/
```

## Remotes

 - [GitHub](https://github.com/englishextra/serguei-englishextra.rhcloud.com)
 - [BitBucket](https://bitbucket.org/englishextra/serguei-englishextra.rhcloud.com)
 - [GitLab](https://gitlab.com/englishextra/serguei-englishextra.rhcloud.com)

## Copyright

© [englishextra.github.com](https://englishextra.github.com/), 2015-2017