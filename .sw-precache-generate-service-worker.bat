sw-precache --root=diy/ --config=./sw-precache-config.js --verbose && ren "%~dp0diy\service-worker.js" "service-worker.min.js" && uglifyjs "%~dp0diy/service-worker.min.js" --compress -o "%~dp0diy/service-worker.min.js"

pause
